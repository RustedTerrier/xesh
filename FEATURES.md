- [X] Command and arguments
- [X] Config Files
- [ ] Shell Specifics
  - [X] Alias
  - [X] Unalias
  - [X] Export
  - [X] Cd
  - [X] Exit
  - [ ] Exec
- [X] Tilde expansion
- [ ] Variables
- [ ] Control flow
  - [ ] if / else
  - [ ] while
  - [ ] for
  - [ ] loop 
  - [ ] case
 - [ ] Strings
 - [ ] Pipes
 - [X] String together commands `cd .. && ls`
