use std::{env, path::Path, process::Command};

use crate::alias;

pub fn set_env_var(var: &str, value: &str) {
    env::set_var(var, value);
}

pub fn run_command<'a>(command: Vec<&'a str>, alias: &Vec<String>, aliasd2: Vec<String>) -> bool {
    let aliasd = aliasd2.clone();
    let child = Command::new(&alias::check_command(&command, alias, &aliasd)[..]).args(alias::check_args(command, alias, aliasd)).spawn();

    match &child {
        | Ok(_child) => {
            // We already made sure it's not an error
            child.unwrap().wait();
            true
        },
        | Err(e) => {
            eprintln!("{}", e);
            false
        }
    }
}

pub fn cd_command(command: Vec<&str>, last_dir: &str) {
    let child;
    if command.len() <= 1 {
        child = env::set_current_dir(Path::new(&env::var("HOME").unwrap()[..]));
    } else {
        match &command[1] {
            | &"-" => child = env::set_current_dir(Path::new(&last_dir)),
            | _ => child = env::set_current_dir(Path::new(command[1]))
        }
    }
    match child {
        | Ok(_child) => {
            child.unwrap();
        },
        | Err(e) => eprintln!("{}", e)
    }
}
