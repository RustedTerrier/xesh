use std::{env, io, io::{stdin, stdout, Write}};

mod config;
mod prompt;
mod command;
pub mod alias;

fn main() {
    let mut xesh = Shell { tried_conf:  false,
                           home:        env::var("HOME").unwrap(),
                           current_dir: get_dir(),
                           last_dir:    LastDir { last_dir: get_dir(), true_last_dir: get_dir() },
                           alias:       Vec::new(),
                           aliasd:      Vec::new() };
    xesh.run().expect("Could not read keys.");
}

fn get_dir() -> String {
    env::current_dir().unwrap().display().to_string()
}

struct LastDir {
    pub last_dir:      String,
    pub true_last_dir: String
}

struct Shell {
    pub tried_conf:  bool,
    pub home:        String,
    pub current_dir: String,
    pub last_dir:    LastDir, // Extra stuff that needs to be saved can be added here
    pub alias:       Vec<String>,
    pub aliasd:      Vec<String>
}

impl Shell {
    pub fn run(&mut self) -> Result<(), io::Error> {
        loop {
            if !self.tried_conf {
                // Get the config and run it. I should probably seperate this into a seperate
                // function
                let cmds = match config::get_config_as_cmd(self.home.clone()) {
                    | Some(a) => a,
                    | None => vec!["".to_string()]
                };
                for i in cmds {
                    let input = i.to_string();
                    self.handle_cmds(input);
                }
                self.tried_conf = true;
            }
            let prompt_char = config::get_prompt_char();
            let prompt = prompt::get_prompt(prompt_char);
            print!("{}", prompt);
            // Flush the output cus it doesn't display this otherwise
            stdout().flush().unwrap();

            // Get input
            let mut input = String::new();
            stdin().read_line(&mut input).unwrap();
            let inputs: Vec<&str> = input.split(';').collect();
            let mut exit = false;
            for i in inputs {
                // We run the commands and check if we have to exit
                exit = self.handle_cmds(i.to_string());
            }
            if exit {
                self.alias = Vec::new();
                self.aliasd = Vec::new();
                break;
            }
        }
        Ok(())
    }

    pub fn handle_cmds(&mut self, input: String) -> bool {
        // For exiting the shell
        let mut exit = false;
        let mut last_cmd_success = true;
        // Better home feature thing
        let input = input.replace("~", &self.home);
        let commands: Vec<&str> = input.trim().split("&&").collect();
        for c in commands.clone() {
            if last_cmd_success {
                if !c.is_empty() {
                    let command: Vec<&str> = c.split_whitespace().collect();
                    match &command[0] {
                        | &"" => {},
                        | &"exit" => {
                            exit = true;
                            break;
                        },
                        | &"export" => {
                            let args: Vec<&str> = command[1].split('=').collect();
                            command::set_env_var(args[0], args[1]);
                        },
                        | &"alias" => {
                            let aliases = alias::alias(&command, self.alias.clone(), self.aliasd.clone());
                            self.alias = aliases.alias;
                            self.aliasd = aliases.aliasd;
                        },
                        | &"unalias" => {
                            let aliases = alias::unalias(command[1].to_string(), self.alias.clone(), self.aliasd.clone());
                            self.alias = aliases.alias;
                            self.aliasd = aliases.aliasd;
                        },
                        | &"cd" => {
                            command::cd_command(command, &self.last_dir.true_last_dir[..]);
                            self.last_dir.true_last_dir = self.last_dir.last_dir.clone();
                            self.last_dir.last_dir = get_dir();
                        },
                        | _ => last_cmd_success = command::run_command(command, &self.alias, self.aliasd.clone())
                    }
                }
            }
        }
        exit
    }
}
