use std::env;

use crate::config;

pub fn get_prompt(prompt: char) -> String {
    let user = env::var("HOME").unwrap();
    let path = env::current_dir().unwrap().display().to_string();
    format!("{} {} ", format_path(path.replace(&user, "~")), prompt)
}

fn get_colors(color: &str) -> String {
    match color {
        | "black" => format!("{}{}", 27 as char, "[38;5;15m"),
        | "red" => format!("{}{}", 27 as char, "[38;5;9m"),
        | "green" => format!("{}{}", 27 as char, "[38;5;10m"),
        | "yellow" => format!("{}{}", 27 as char, "[38;5;11m"),
        | "blue" => format!("{}{}", 27 as char, "[38;5;12m"),
        | "magenta" => format!("{}{}", 27 as char, "[38;5;13m"),
        | "cyan" => format!("{}{}", 27 as char, "[38;5;14m"),
        | "white" => format!("{}{}", 27 as char, "[38;5;15m"),
        | "reset" => format!("{}{}", 27 as char, "[39m"),
        | _ => format!("{}{}", 27 as char, "[38;5;10m")
    }
}

fn format_path(path: String) -> String {
    let color = &config::get_color()[..];
    let paths: Vec<&str> = path.split('/').collect();
    let mut path_new = "".to_string();
    for i in paths {
        if path_new != *"" {
            path_new = format!("{}{}/{}{}", path_new, get_colors("reset"), get_colors(color), i);
        } else {
            path_new = format!("{}{}", get_colors(color), i);
        }
    }
    format!("{}{}", path_new, get_colors("reset"))
}
