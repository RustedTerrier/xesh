pub fn check_command(command: &Vec<&str>, alias: &Vec<String>, aliasd: &Vec<String>) -> String {
    let mut command_real = command[0].to_string();
    let mut j = 0;
    for i in alias {
        if &command_real == i {
            let vec: Vec<&str> = aliasd[j].split_whitespace().collect();
            command_real = vec[0].to_string();
        }
        j += 1;
    }
    command_real
}

pub fn check_args<'a>(command: Vec<&'a str>, alias: &Vec<String>, aliasd: Vec<String>) -> Vec<String> {
    let command_real = command[0].to_string();
    let mut args2 = command.clone();
    let mut j = 0;
    for i in alias {
        if &command_real == i {
            let vec: Vec<&str> = aliasd[j].split_whitespace().collect();
            if vec.len() > 1 {
                for k in 1 ..= vec.len() - 1 {
                    args2.push(vec[k]);
                }
            }
        }
        j += 1;
    }
    args2.remove(0);
    let mut args: Vec<String> = Vec::new();
    for i in args2 {
        args.push(i.to_string());
    }
    args
}

pub fn unalias(alias_string: String, alias2: Vec<String>, aliasd2: Vec<String>) -> Alias {
    let mut alias = alias2;
    let mut aliasd = aliasd2;
    for j in 0 .. alias.len() {
        if alias[j] == alias_string {
            alias.remove(j);
            aliasd.remove(j);
            // Not sure why I have to but it freaks out otherwise...
            break;
        }
    }
    Alias { alias, aliasd }
}

pub fn alias(alias_string: &Vec<&str>, alias2: Vec<String>, aliasd2: Vec<String>) -> Alias {
    let mut alias = alias2;
    let mut aliasd = aliasd2;
    let mut whole_command = alias_string.to_owned();
    whole_command.remove(0);
    let mut whole_cmd_str = whole_command[0].to_string();
    if whole_command.len() > 1 {
        for i in 1 .. whole_command.len() {
            whole_cmd_str = format!("{} {}", whole_cmd_str, whole_command[i]);
        }
    }
    let alias_string: Vec<&str> = whole_cmd_str.split("=\"").collect();
    alias.push(alias_string[0].to_owned());
    let mut aliasd_string_improved = alias_string[1].trim_end().to_owned();
    if aliasd_string_improved.ends_with(";") {
        aliasd_string_improved = aliasd_string_improved.trim_end().to_owned();
    }
    aliasd.push(aliasd_string_improved);
    Alias { alias, aliasd }
}

pub struct Alias {
    pub alias:  Vec<String>,
    pub aliasd: Vec<String>
}
