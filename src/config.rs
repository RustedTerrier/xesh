use std::{env, fs, path::Path};

pub fn get_config_as_cmd<'a>(home: String) -> Option<Vec<String>> {
    let path = format!("{}/.config/xesh/config.xesh", home);
    if !Path::new(&path).exists() {
        let path = format!("{}/.xeshrc", home);
        if !Path::new(&path).exists() {
            None
        } else {
            let mut file = fs::read_to_string(&path).unwrap();
            file = file.replace('\n', "");
            let cmds: Vec<&str> = file.split(';').collect();
            let mut cmds_s: Vec<String> = Vec::new();
            for i in cmds {
                cmds_s.push(i.to_string());
            }
            Some(cmds_s)
        }
    } else {
        let mut file = fs::read_to_string(&path).unwrap();
        file = file.replace('\n', "");
        let cmds: Vec<&str> = file.split(';').collect();
        let mut cmds_s: Vec<String> = Vec::new();
        for i in cmds {
            cmds_s.push(i.to_string());
        }
        Some(cmds_s)
    }
}

pub fn get_prompt_char() -> char {
    let env = match env::var("XESH_PROMPT") {
        | Ok(prompt) => prompt.replace(" ", ""),
        | Err(_) => ">".to_string()
    };
    let chars: Vec<char> = env.chars().collect();
    chars[0]
}

pub fn get_color() -> String {
    let env = match env::var("XESH_COLOR") {
        | Ok(prompt) => prompt.replace(" ", ""),
        | Err(_) => "green".to_string()
    };
    env
}
